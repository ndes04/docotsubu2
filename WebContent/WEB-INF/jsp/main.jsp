<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.User,model.Mutter,java.util.List" %>
    <%
    //セッションスコープに保存されたユーザー情報を取得
    User loginUser =(User) session.getAttribute("loginUser");
    //アプリケーションスコープに保存されたユーザー情報を取得
    List<Mutter> mutterList =
    (List<Mutter>)request.getAttribute("mutterList");
    //リクエストスコープに保存されたエラーメッセージを取得
    String errorMsg =(String)request.getAttribute("errorMsg");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>どこつぶ</title>
</head>
<body>
<h1>どこつぶメイン</h1>
<p>
<%=loginUser.getName() %>さん、ログイン中
<a href="/docoTsubu2/Logout">ログアウト</a>
</p>
<p><a href="/docoTsubu2/Main">更新</a></p>
<form action="/docoTsubu2/Main" method="post">
<input type="text" name="text">
<button type="submit">つぶやく</button>
</form>
<%if(errorMsg != null){ %>
<p><%=errorMsg %></p>
<%} %>
<%for(Mutter mutter : mutterList){%>
<p><%=mutter.getUserName() %>:<%=mutter.getText() %></p>
<%} %>
</body>
</html>