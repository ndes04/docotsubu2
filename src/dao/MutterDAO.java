package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Mutter;

public class MutterDAO {
	//データベース接続に使用する情報

final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
        final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
        final String DB_NAME = "docoTsubu";//データベース名
        final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
        final String JDBC_URL =DB_URL+DB_NAME+DB_ENCODE;//接続DBとURL
        final String DB_USER = "root";//ユーザーID
        final String DB_PASS = "root";//パスワード

        public List<Mutter> findAll(){

        	List<Mutter>mutterList = new ArrayList<>();
        	try {
				Class.forName(DRIVER_NAME);
			} catch (ClassNotFoundException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
        	try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){


        	String sql="SELECT ID,NAME,TEXT FROM MUTTER ORDER BY ID DESC";
        	PreparedStatement pStmt = conn.prepareStatement(sql);

        	//SELECTを実行
        	ResultSet rs = pStmt.executeQuery();

        	//SELECT文の結果をArrayListに格納
        	while(rs.next()){
        		int id = rs.getInt("ID");
        		String userName = rs.getString("NAME");
        		String text = rs.getString("TEXT");
        		Mutter mutter = new Mutter(id,userName,text);
        		mutterList.add(mutter);
        	}
        }catch(SQLException e){
        	e.printStackTrace();
        	return null;

		}
        	return mutterList;
        }
        public boolean create(Mutter mutter){
        	//データベース接続
        	try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){


        	//INSERT文の準備(idは自動連番なので指定しなくてよい)
        	String sql = "INSERT INTO MUTTER(NAME,TEXT) VALUES(?,?)";
        	PreparedStatement pStmt = conn.prepareStatement(sql);

        	//INSERT文中の「？」に使用する値を設定しSQLを完成
        	pStmt.setString(1, mutter.getUserName());;
        	pStmt.setString(2, mutter.getText());

        	//INSERT文を実行(resultには追加された行数が代入される)
        	int result = pStmt.executeUpdate();
        	if(result != 1){
        		return false;

        	}
        }catch(SQLException e){
        	e.printStackTrace();
        	return false;

        }
        	return true;

}
}
